import com.google.gson.Gson;
import spark.Response;

class ErrorResponse {
  private final String message;

  ErrorResponse(String message) {
    this.message = message;
  }

  /**
   * Format an error response as JSON, and set the status code in `response`
   * to `httpStatusCode`
   *
   * @param response Response to set status code on
   * @param httpStatusCode Status code to set in `response`
   * @param message  Message to return in JSON body
   * @return JSON response as a string
   */
  public static String ErrorResponseAsJson(Response response,
                                           int httpStatusCode,
                                           String message) {
    response.status(httpStatusCode);
    Gson gson = new Gson();
    return gson.toJson(new ErrorResponse(message));
  }
}
