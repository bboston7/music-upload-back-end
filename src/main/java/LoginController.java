import org.dom4j.Node;
import spark.Request;
import spark.Response;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import spark.Session;

public class LoginController {

  public static Object handleLogin(Request request, Response response) {
    String username = request.queryParams("username");
    String password = request.queryParams("password");

    if (username == null || password == null) {
      return ErrorResponse.ErrorResponseAsJson(
          response,
          HttpURLConnection.HTTP_BAD_REQUEST,
          "Missing username and/or password request parameters.");
    }

    try {
      // Forward request to wmbr login server
      URL url = new URL("https://wmbr.org/cgi-bin/member_auth");
      HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
      httpConnection.setRequestMethod("POST");
      httpConnection.setDoOutput(true);
      StringBuilder builder = new StringBuilder("u=");
      builder.append(URLEncoder.encode(username, "UTF-8"));
      builder.append("&p=");
      builder.append(URLEncoder.encode(password, "UTF-8"));
      byte[] login_bytes = builder.toString().getBytes(StandardCharsets.UTF_8);
      httpConnection.setFixedLengthStreamingMode(login_bytes.length);
      httpConnection.connect();
      httpConnection.getOutputStream().write(login_bytes);

      // TODO: Does the service ever return anything other than 200?  It seems
      //  like even bad requests return 200
      if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
        // TODO: Log
        return ErrorResponse.ErrorResponseAsJson(
            response,
            HttpURLConnection.HTTP_BAD_GATEWAY,
            "Unexpected status code from login service");
      }

      // Read response and parse xml
      // TODO: Process the XML at this input stream
      InputStream login_input = httpConnection.getInputStream();
      Document dom = new SAXReader().read(login_input);

      // TODO: Keep going from https://stackoverflow.com/questions/3324717/sending-http-post-request-in-java
      List<Node> errors = dom.selectNodes("//errormsg");
      if (!errors.isEmpty()) {
        // We sent a bad request
        // TODO: Log the text of `errors` (that's why I used a list and not
        //  `dom.matches()`)
        return ErrorResponse.ErrorResponseAsJson(
            response,
            HttpURLConnection.HTTP_BAD_GATEWAY,
            "Received error from login service.");
      }

      if (dom.selectSingleNode("//invalid") != null) {
        // Bad username/password combo
        return ErrorResponse.ErrorResponseAsJson(
            response,
            HttpURLConnection.HTTP_FORBIDDEN,
            "Bad username/password combo");
      }

      Node firstname = dom.selectSingleNode("//firstname");
      Node lastname = dom.selectSingleNode("//lastname");
      Node member_id = dom.selectSingleNode("//member_id");
      if (firstname == null || lastname == null || member_id == null) {
        // TODO: Log
        return ErrorResponse.ErrorResponseAsJson(
            response,
            HttpURLConnection.HTTP_BAD_GATEWAY,
            "Received unexpected response from login service.");
      }

      // Login success!  Make a session
      Session session = request.session(true);
      session.attribute("name",
                        firstname.getText() + " " + lastname.getText());
      session.attribute("member_id", member_id.getText());
    } catch (IOException | DocumentException e) {
      // TODO: Log and remove exception message from response
      // TODO: More fine grained exception handling
      return ErrorResponse.ErrorResponseAsJson(
          response,
          HttpURLConnection.HTTP_INTERNAL_ERROR,
          e.getMessage());
    }

    response.status(HttpURLConnection.HTTP_OK);
    return "Success";
  }
}
