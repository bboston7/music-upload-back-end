import com.google.gson.Gson;

import org.apache.tika.Tika;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import spark.Request;
import spark.Response;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class UploadController {

  /**
   * Directory on disk to place uploaded files
   */
  // TODO: Set me with a config file instead
  private static final String UPLOAD_DIR = "/tmp/wmbr-uploads";
  private static final Gson gson = new Gson();
  private static final long MIN_BITRATE = 256;

  // TODO: Does this mapping exist anywhere in Tika?
  private static final Map<String, String> MIME_EXTENSION_MAP;
  static {
    Map<String, String> m = new HashMap<>();
    m.put("audio/x-flac", ".flac");
    m.put("audio/mpeg", ".mp3");
    MIME_EXTENSION_MAP = Collections.unmodifiableMap(m);
  }

  private static String InternalServerError(Response response) {
    return ErrorResponse.ErrorResponseAsJson(
        response,
        HttpURLConnection.HTTP_INTERNAL_ERROR,
        "Internal server error");
  }

  private static String UserError(Response response, String message) {
    return ErrorResponse.ErrorResponseAsJson(
        response,
        HttpURLConnection.HTTP_BAD_REQUEST,
        message);
  }

  public static Object handleOptions(Request request, Response response) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Methods", "POST");
    return "";
  }

  // TODO: To prevent people from reuploading, store hashes in db and reject
  //  files with matching hashes (or return 200 and silently deduplicate)
  public static Object handleUpload(Request request, Response response) {
    // Ensure response doesn't violate CORS rules by allowing it to be read
    // by anyone
    // TODO: Narrow this scope
    response.header("Access-Control-Allow-Origin", "*");

    // Configure Jetty server to place uploaded files in UPLOAD_DIR
    new File(UPLOAD_DIR).mkdir();
    MultipartConfigElement config = new MultipartConfigElement(UPLOAD_DIR);
    request.raw().setAttribute("org.eclipse.jetty.multipartConfig",
                               config);

    String output_path = "failed";
    try {
      // Get "song" field from  multipart upload
      // TODO: Can use request.raw().getParts to get a collection of parts
      //  See javax.servlet.http for more info about the object returned by
      //  request.raw()
      Part song = request.raw().getPart("song");

      // Create a unique file to write the song out to
      // TODO: Put temporary files in another directory
      File outdir = new File(UPLOAD_DIR);
      File untyped_temp = File.createTempFile("untyped-upload",
                                              ".tmp",
                                              outdir);
      song.write(untyped_temp.getName());

      // Detect file type and move file to match extension because
      // Jaudiotagger stupidly uses extensions instead of MIME types to
      // dispatch a parser
      String mime = new Tika().detect(untyped_temp);
      if (!MIME_EXTENSION_MAP.containsKey(mime)) {
        // Unsupported media type
        untyped_temp.delete();
        return UserError(response, "Unsupported media type: " + mime);
      }
      // Move to new file
      File typed_temp = File.createTempFile("upload",
                                            MIME_EXTENSION_MAP.get(mime),
                                            outdir);
      if (!untyped_temp.renameTo(typed_temp)) {
        // TODO: Log
        return InternalServerError(response);
      }
      output_path = typed_temp.getName();
      AudioFile f = AudioFileIO.read(typed_temp);
      AudioHeader header = f.getAudioHeader();
      long bitrate = header.getBitRateAsNumber();
      if (bitrate < MIN_BITRATE) {
        typed_temp.delete();
        return UserError(response, String.format(
            "Bit rate too low.  Bit rate must be greater than or equal to " +
                "%dkbps, but your file has a bit rate of %dkbps.",
            MIN_BITRATE,
            bitrate));
      }

      Tag tag = f.getTag();
      // TODO: Extract bitrate and format from here

      UploadResponse ret = new UploadResponse(tag.getFirst(FieldKey.ARTIST),
                                              tag.getFirst(FieldKey.TITLE),
                                              output_path);

      return gson.toJson(ret);

      // TODO: Delete temp file on failure
    } catch (IOException | ServletException | ReadOnlyFileException e) {
      // Internal server error
      return InternalServerError(response);
    } catch (CannotReadException |
             TagException |
             InvalidAudioFrameException e) {
      // User error
      return UserError(response, e.getMessage());
    }
  }
}
