class UploadResponse {
  private final String artist;
  private final String title;
  private final String path;

  UploadResponse(String artist, String title, String path) {
    this.artist = artist;
    this.title = title;
    this.path = path;
  }
}
