import static spark.Spark.*;

public class Uploader {
  public static void main(String[] args) {
    // Bind server to port 8888
    port(8888);

    // Register upload handler
    post("/upload", UploadController::handleUpload);
    options("/upload", UploadController::handleOptions);

    // Register login service
    post("/login", LoginController::handleLogin);
  }
}
